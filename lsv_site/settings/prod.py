import environ

from lsv_site.settings.base import *

env = environ.Env()
env.read_env(env_file=os.path.join(BASE_DIR, '.env'))


DEBUG = False
SECRET_KEY = env('SECRET_KEY')

CSRF_COOKIE_SECURE = True

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS')

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(env('REDIS_SERVER'), env('REDIS_PORT'))],
        },
    },
}

#Logging settings
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[{asctime}] [{levelname}] {message}',
            'style': '{',
            'datefmt': '%d/%b/%Y %H:%M:%S',
        }
    },
    'handlers': {
        'request_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(env('LOG_DIRECTORY'), 'lsv_requests.log'),
            'formatter': 'default',
        },
        'security_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(env('LOG_DIRECTORY'), 'lsv_security.log'),
            'formatter': 'default',
        },
        'database_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(env('LOG_DIRECTORY'), 'lsv_database.log'),
            'formatter': 'default',
        },
    },
    'root': {
        'handlers': ['request_file'],
        'level': 'DEBUG',
    },
    'loggers': {
        'django.request': {
            'handlers': ['request_file'],
            'level': 'DEBUG',
        },
        'django.security': {
            'handlers': ['security_file'],
            'level': 'DEBUG',
        },
        'django.db.backends': {
            'handlers': ['database_file'],
            'level': 'DEBUG',
        },
    }
}
