from lsv_site.settings.base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

STATIC_ROOT = None
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'home/static'),
    os.path.join(BASE_DIR, 'rollchat/static'),
]

#Mail settings
EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
EMAIL_FILE_PATH = os.path.join(BASE_DIR, "sent_emails")

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("192.168.1.21", 6379)],
        },
    },
}
