from django.apps import AppConfig


class NovlogConfig(AppConfig):
    name = 'novlog'

    def ready(self):
        import novlog.signals
