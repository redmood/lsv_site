from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User


class Novel(models.Model):
    author=models.ForeignKey(User, on_delete=models.CASCADE)
    title=models.CharField(max_length=255, null=False, blank=False)
    slug=models.SlugField(unique=True, max_length=255, null=True, blank=True)
    summary=models.TextField(max_length=255, null=True, blank=True)
    url=models.URLField(max_length=255)
