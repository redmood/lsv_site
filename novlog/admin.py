from ckeditor.widgets import CKEditorWidget

from django.contrib import admin
from django.db import models
from django.http import HttpResponseRedirect

from novlog.models import Novel


@admin.register(Novel)
class NovelAdmin(admin.ModelAdmin):
    list_display = [
        'title',
        'author',
    ]

    fieldsets = (
        ('Info', {
            'fields': ('author', 'title', 'summary', 'url')
        }),
    )
