from slugify import slugify

from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.timezone import now

from novlog.models import Novel


@receiver(pre_save, sender=Novel)
def send_update_to_monitor_pre_save(sender, instance, **kwargs):
    if instance.title:
        instance.slug = slugify(instance.title)

    instance.last_update_date = now()
