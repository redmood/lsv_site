from django.shortcuts import render
from django.views.generic import ListView

from novlog.models import Novel


class NovelListView(ListView):
    model = Novel
    queryset = Novel.objects.all()
