from django.urls import path
from novlog import views

app_name = 'novlog'

urlpatterns = [
    path('', views.NovelListView.as_view(), name='listnovels'),
]
