from django.urls import path

from . import views

app_name = 'rollchat'

urlpatterns = [
    path('', views.RollchatRoomListView.as_view(), name='index'),
    path('rooms/<slug:slug>/', views.RollchatRoomDetailView.as_view(), name='room'),
]
