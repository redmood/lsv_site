from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.urls import reverse_lazy
from django.db.models import Q

from rollchat.models import RollchatRoom


class RollchatRoomListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')
    model = RollchatRoom
    template_name = 'rollchat/chat-index.html'

    def get_queryset(self):
        queryset = RollchatRoom.objects.all()

        if self.request.user.is_superuser:
            return queryset

        user = self.request.user
        user_rooms = user.owns_rooms.all().values_list('id', flat=True)
        user_rooms |= user.masters_rooms.all().values_list('id', flat=True)
        user_rooms |= user.in_rooms.all().values_list('id', flat=True)
        return queryset.filter(id__in=user_rooms)


class RollchatRoomDetailView(LoginRequiredMixin, DetailView):
    model = RollchatRoom
    template_name = 'rollchat/chat-room.html'

    def get_context_data(self, **kwargs):
        context = super(RollchatRoomDetailView, self).get_context_data(**kwargs)
        context['room_name'] = self.kwargs['slug']
        context['username'] = self.request.user.username
        return context
