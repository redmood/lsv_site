let usernames = Array()

function history(messages) {
  console.log(messages);

  usernames_list = messages.members;
  console.log(usernames_list);
  messages_list = messages.messages;
  console.log(messages_list);

  for(i = 0; i < usernames_list.length; i++) {
    add_user_in_list(usernames_list[i]);
  }

  for(i = 0; i < messages_list.length; i++) {
    new_chat_message(messages_list[i]);
  }
}

function add_user_in_list(username) {
  var users_container = document.querySelector('.usersbox ul');

  known = false;
  if(usernames.length > 0) {
    for(i = 0; i < usernames.length; i++) {
      if(usernames[i] == username) {
        known = true;
        break;
      };
    };
  };

  if(!known) {
    new_user = document.createElement('li');
    new_user.class = 'username';
    new_user.innerHTML = username;
    users_container.appendChild(new_user);
}

  usernames.push(username);
};

function new_chat_message(data) {
  username = data.username;
  data_content = data.content;
  var table = document.querySelector('table');

  var line = document.createElement('tr');
  line.className = "tab-row chatmessage";

  var user_column = document.createElement('td');
  user_column.className = "username";
  user_column.innerHTML = username;
  line.appendChild(user_column);

  var message_column = document.createElement('td');
  message_column.className = "message";

  var message = document.createElement('p');
  message.className = "message-line";
  message.innerHTML = data_content.join('</br>');
  message_column.appendChild(message);

  line.appendChild(message_column);
  table.appendChild(line);
};

var self = document.querySelector('#user_name').innerHTML;

const roomName = document.getElementById('room-name').textContent;

const chatSocket = new ReconnectingWebSocket(
    'ws://'
    + window.location.host
    + '/ws/rollchat/'
    + roomName
    + '/'
);

chatSocket.onmessage = function(e) {
    const data = JSON.parse(e.data);
    console.log(data)

    if(data.message.type == 'message') {
      new_chat_message(data.message);
    } else if(data.message.type == 'history') {
      history(data.message);
    }
};

chatSocket.onclose = function(e) {
    console.error('Chat socket closed unexpectedly');
};

document.querySelector('#chat-message-input').focus();
document.querySelector('#chat-message-input').onkeyup = function(e) {
    if (e.keyCode === 13) {  // enter, return
        document.querySelector('#chat-message-submit').click();
    }
};

document.querySelector('#chat-message-submit').onclick = function(e) {
    const messageInputDom = document.querySelector('#chat-message-input');
    const message = messageInputDom.value;
    chatSocket.send(JSON.stringify({
        'message': message
    }));
    messageInputDom.value = '';
};
