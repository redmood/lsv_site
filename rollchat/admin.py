from django.contrib import admin
from django.contrib.auth.models import User

from rollchat.models import RollchatRoom
from rollchat.models import ChatRoomHistory


@admin.register(RollchatRoom)
class RollchatRoomAdmin(admin.ModelAdmin):
    list_display = [
        'slug',
        'room_admin',
        'room_master',
    ]

    fiels = [
        'slug',
        'room_admin',
        'room_master',
        'room_members',
    ]

    filter_vertical = ['room_members']

@admin.register(ChatRoomHistory)
class ChatRoomHistoryAdmin(admin.ModelAdmin):
    list_display = [
        'user',
        'room',
        'sent_on',
    ]

    fields = ('user', 'room', 'content', 'sent_on')
