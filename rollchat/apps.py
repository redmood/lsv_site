from django.apps import AppConfig


class RollchatConfig(AppConfig):
    name = 'rollchat'
