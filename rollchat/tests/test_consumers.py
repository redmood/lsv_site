import json

from asgiref.sync import AsyncToSync

from unittest.mock import patch, MagicMock
from django.test import TestCase

from rollchat.consumers import RollchatConsumer
from rollchat.tests.tools import (
    UserFactory,
    RollchatRoomFactory,
    ChatRoomHistoryFactory
)


class RollchatConsumerTestCase(TestCase):

    def setUp(self):
        self.adminuser = UserFactory(username='admin')
        self.adminuser.save()

    def test_scope_get_room_slug(self):
        consumer = RollchatConsumer(scope={'path': 'test_path'})
        room_slug = consumer.scope_get_room_slug()
        self.assertEqual(room_slug, 'test_path')

    def test_scope_get_username(self):
        consumer = RollchatConsumer(scope={'user': 'username'})
        username = consumer.scope_get_username()
        self.assertEqual(username, 'username')

    @patch.object(AsyncToSync, '__call__')
    def test_receive_classic_text(self, mock_async_to_sync):
        user = UserFactory()
        user.save()
        room = RollchatRoomFactory(room_admin=self.adminuser)
        room.save()

        consumer = RollchatConsumer(scope={'user': user.username, 'path': room.slug})
        consumer.add_history_entry = MagicMock(name='add_history_entry')
        consumer.channel_layer = MagicMock(name='channel_layer')
        consumer.channel_layer.group_send = MagicMock(name='group_send')
        consumer.room_group_name = f'rollchat_{room.slug}'

        message = {
            'message': 'Lorem ipsum\nVulnera Sanentur',
        }
        consumer.receive(json.dumps(message))

        response = {
            'type': 'send_chat_message',
            'message': {
                'type': 'message',
                'content': ['Lorem ipsum', 'Vulnera Sanentur'],
                'username': user.username,
            }
        }

        consumer.add_history_entry.assert_called_once_with(
            user,
            room,
            message['message'],
        )
        mock_async_to_sync.assert_called_once_with(
            consumer.room_group_name,
            response,
        )

    @patch.object(AsyncToSync, '__call__')
    def test_receive_roll(self, mock_async_to_sync):
        user = UserFactory()
        user.save()
        room = RollchatRoomFactory(room_admin=self.adminuser)
        room.save()

        consumer = RollchatConsumer(scope={'user': user.username, 'path': room.slug})
        consumer.add_history_entry = MagicMock(name='add_history_entry')
        consumer.channel_layer = MagicMock(name='channel_layer')
        consumer.channel_layer.group_send = MagicMock(name='group_send')
        consumer.room_group_name = f'rollchat_{room.slug}'

        message = {
            'message': '#2d20',
        }

        with patch('rollchat.dice.roll', return_value=(15, ['5/20', '10/20'])) as mock_dice:
            consumer.receive(json.dumps(message))

        response = {
            'type': 'send_chat_message',
            'message': {
                'type': 'message',
                'content': ['a lancé:', 'Un dé 20, résultat: 5', 'Un dé 20, résultat: 10'],
                'username': user.username,
            }
        }

        consumer.add_history_entry.assert_called_once_with(
            user,
            room,
            'a lancé:\nUn dé 20, résultat: 5\nUn dé 20, résultat: 10',
        )
        mock_async_to_sync.assert_called_once_with(
            consumer.room_group_name,
            response,
        )
