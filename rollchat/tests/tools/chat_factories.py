import factory

from rollchat.models import RollchatRoom, ChatRoomHistory


class RollchatRoomFactory(factory.Factory):
    class Meta:
        model = RollchatRoom

    slug = factory.Sequence(lambda n: f'room_{n}')
    max_players = 3


class ChatRoomHistoryFactory(factory.Factory):
    class Meta:
        model = ChatRoomHistory

    content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus."
