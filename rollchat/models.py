from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User


class RollchatRoom(models.Model):
    slug = models.SlugField(max_length=150)
    max_players = models.PositiveSmallIntegerField(default=6)
    room_admin = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owns_rooms')
    room_master = models.ForeignKey(User, on_delete=models.CASCADE, related_name='masters_rooms', null=True, blank=True)
    room_members = models.ManyToManyField(User, related_name='in_rooms', blank=True)

    def add_user(self, user):
        self.room_members.add(user)

    def remove_user(self, user):
        self.room_members.remove(user)

    def __str__(self):
        return f'{self.slug}'


class ChatRoomHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_messages')
    room = models.ForeignKey(RollchatRoom, on_delete=models.CASCADE, related_name='messages')
    content = models.TextField(null=True, blank=True)
    sent_on = models.DateTimeField(null=False, blank=False, default=now)
