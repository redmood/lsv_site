import copy
import json
import logging
from datetime import datetime
from datetime import timedelta

from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User

from rollchat.models import RollchatRoom
from rollchat.models import ChatRoomHistory
from rollchat.dice import roll

logger = logging.getLogger(__name__)

class RollchatConsumer(WebsocketConsumer):

    def scope_get_room_slug(self):
        url_parts = self.scope['path'].split('/')
        if len(url_parts[-1]) > 0:
            return url_parts[-1]

        return url_parts[-2]

    def scope_get_username(self):
        return self.scope['user']

    def connect(self):
        username = self.scope_get_username()
        logger.debug(f'User {username} is connecting...')
        room_slug = self.scope_get_room_slug()
        logger.debug(f'In room {room_slug}...')
        self.room_group_name = f'rollchat_{room_slug}'
        logger.debug(f'User will be added in group {self.room_group_name}')
        self.user_group_name = f'rollchat_user_{username}'

        room = self.get_room_by_slug(room_slug)
        if not room:
            return

        user = self.get_user_by_username(username)
        if not user:
            return

        if not self.user_allowed_in_room(user, room):
            return

        #User connection is accepted
        async_to_sync(self.accept())

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        #Personal user group
        async_to_sync(self.channel_layer.group_add)(
            self.user_group_name,
            self.channel_name,
        )

        message = {
            'type': 'send_chat_message',
            'message': {
                'type': 'history',
                'members': [],
                'messages': []},
        }

        message.get('message').get('members').append(f'{room.room_admin.username} (admin)')
        if room.room_admin != room.room_master:
            message.get('message').get('members').append(f'{room.room_master.username} (master)')
        message.get('message').get('members').extend([member.username for member in room.room_members.all()])

        room_history = self.get_history(room.slug)
        for item in room_history:
            message.get('message').get('messages').append({
                'username': item.user.username,
                'content': list(filter(lambda a : a != "", item.content.split('\n'))),
                'sent_on': str(item.sent_on),
            })

        logger.debug(f'sending message: {message}')
        async_to_sync(self.channel_layer.group_send)(self.user_group_name, message)

        message_dict = {
            'type': 'send_chat_message',
            'message': {
                'type': 'user_entered_room',
                'username': user.username,
            },
        }
        async_to_sync(self.channel_layer.group_send)(self.room_group_name, message_dict)

    def disconnect(self, close_code):
        username = self.scope_get_username()
        slug = self.scope_get_room_slug()

        user = self.get_user_by_username(username)
        room = self.get_room_by_slug(slug)
        room.remove_user(user)

        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

        message_dict = {
            'type': 'send_chat_message',
            'message': {
                'type': 'user_left_room',
                'username': user.username,
            },
        }
        #Acknowlegde other users
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            message_dict,
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        username = self.scope_get_username()
        slug = self.scope_get_room_slug()
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        user = self.get_user_by_username(username)
        room = self.get_room_by_slug(slug)

        broadcast = ""
        if message.startswith('#'):
            logger.debug(message)
            _, res = roll(message.replace("#", ""))
            broadcast = "a lancé:\n"

            for s in res:
                v, n = s.split('/')
                broadcast = broadcast + "Un dé {}, résultat: {}\n".format(n,v)
        else:
            broadcast = message

        #adding message to history
        item = self.add_history_entry(user, room, broadcast)

        message_dict = {
            'type': 'send_chat_message',
            'message': {
                'type': 'message',
                'content': list(filter(lambda a : a != "", broadcast.split('\n'))),
                'username': user.username,
            },
        }
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            message_dict,
        )

    # Receive message from room group
    def send_chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))

    def get_user_by_username(self, username):
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist as e:
            logger.exception(e)
            return None

    def get_room_by_slug(self, slug):
        try:
            return RollchatRoom.objects.get(slug=slug)
        except RollchatRoom.DoesNotExist as e:
            logger.exception(e)
            return None

    def add_history_entry(self, user, room, message):
        return ChatRoomHistory.objects.create(user=user, room=room, content=message)

    def get_history(self, slug):
        return ChatRoomHistory.objects.filter(room__slug=slug).filter(sent_on__gte=datetime.now() - timedelta(hours=24)).order_by('sent_on')

    def user_allowed_in_room(self, user, room):
        if user.is_superuser:
            return True

        if user == room.room_admin:
            return True

        if user == room.room_master:
            return True

        if user in room.room_members.all():
            return True

        return False
