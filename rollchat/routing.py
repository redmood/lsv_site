from django.urls import path

from rollchat.consumers import RollchatConsumer

websocket_urlpatterns = [
    path('ws/rollchat/<str:room_name>/', RollchatConsumer),
]
