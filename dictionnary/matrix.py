# -*- coding: utf-8 -*-

#import os
import numpy as np
import re
import codecs
import csv
from dictionnary.converter import *

M_SIZE = NB_SOUNDS + 2

def generate_matrix(words,codec="utf-8"):

    count = np.zeros((M_SIZE,M_SIZE,M_SIZE),dtype='int32')
    res = []

    for word in words:
        sounds = cast_to_sounds(word)
        i=0
        j=0
        for k in sounds:
            count[i,j,k] += 1
            i = j
            j = k
    return build_prob_matrix(count)

def build_prob_matrix(matrix,m_size=NB_SOUNDS+2):

    s=matrix.sum(axis=2)
    st=np.tile(s.T,(m_size,1,1)).T
    p=matrix.astype('float')/st
    p[np.isnan(p)]=0

    return p
