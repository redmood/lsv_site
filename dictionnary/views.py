import logging
import csv

from django.shortcuts import render
from django.http import Http404
from django.utils.translation import ugettext as _
from django.views.generic.edit import FormMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from dictionnary.models import Word
from dictionnary.forms import SearchWordForm, GeneratorForm
from dictionnary import matrix as mx
from dictionnary import generator as gen

logging.basicConfig(level=logging.DEBUG)
_logger = logging.getLogger(__name__)

class FormListView(FormMixin, ListView):
    def get(self, request, *args, **kwargs):
        # From ProcessFormMixin
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        # From BaseListView
        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()
        if not allow_empty and len(self.object_list) == 0:
            raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
                          % {'class_name': self.__class__.__name__})

        context = self.get_context_data(object_list=self.object_list, form=self.form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class WordListView(FormListView):
    form_class = SearchWordForm
    model = Word
    template_name = 'dictionnary/word_list_view.html'
    queryset = Word.objects.all().order_by('morwuwhw')
    paginate_by = 20

    def load_file_data(self):
        with open('../saved_data/words-data.csv') as f:
                reader = csv.reader(f)

                for row in reader:
                    _logger.debug("french: {}, morwuwhw: {}, description: {}".format(row[0],row[1],row[2]))
                    _, created = Word.objects.get_or_create(
                        french=row[0],
                        morwuwhw=row[1],
                        description=row[2],
                        )
                    # creates a tuple of the new object or
                    # current object and a boolean of if it was created

    def get_context_data(self, **kwargs):
        context = super(WordListView, self).get_context_data()
        context['word_count'] = Word.objects.count()
        return context


class WordDetailView(DetailView):
    model = Word
    template_name = 'dictionnary/word_detail_view.html'


def search_word(request):
    search_form = SearchWordForm()

    if request.method == 'POST':
        search_form = SearchWordForm(request.POST)

        if search_form.is_valid():
            result = None
            if search_form.cleaned_data['language'] == SearchWordForm.MORWUWHW:
                result = Word.objects.filter(morwuwhw=search_form.cleaned_data['word']).order_by('morwuwhw')
            elif search_form.cleaned_data['language'] == SearchWordForm.FRANCAIS:
                result = Word.objects.filter(french=search_form.cleaned_data['word']).order_by('french')
            else:
                return render(request,'dictionnary/word_list_view.html')

            if len(result) == 0:
                if search_form.cleaned_data['language'] == SearchWordForm.MORWUWHW:
                    result = Word.objects.filter(morwuwhw__icontains=search_form.cleaned_data['word']).order_by('morwuwhw')
                elif search_form.cleaned_data['language'] == SearchWordForm.FRANCAIS:
                    result = Word.objects.filter(french__icontains=search_form.cleaned_data['word']).order_by('french')
                else:
                    return render(request,'dictionnary/word_list_view.html')

            return render(request,'dictionnary/search_result.html',{'word_list':result})
        else:
            return render(request,'dictionnary/word_list_view.html')

def generator_form_view(request):
    form = GeneratorForm()

    if request.method == 'POST':
        form = GeneratorForm(request.POST)

        if form.is_valid():
            examples_words = Word.objects.values_list('morwuwhw')
            words = []
            for word in examples_words:
                words.append(word[0])
            matrix = mx.generate_matrix(words)
            gen_words = gen.generate_words(
                max_mots=form.cleaned_data['max_words'],
                word_size=form.cleaned_data['size_words'],
                p=matrix,
                in_database_words=words
            )
            return render(request,'dictionnary/generator_form.html',{'gen_words':gen_words})
    return render(request,'dictionnary/generator_form.html',{'form':form})
