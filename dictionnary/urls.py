from django.urls import path
from dictionnary import views

app_name = 'dictionnary'

urlpatterns = [
    path('', views.WordListView.as_view(), name='listword'),
    path('detailword/<int:pk>', views.WordDetailView.as_view(), name='detailword'),
    path('searchword/', views.search_word,name='searchword'),
    path('generator/', views.generator_form_view, name='setgenerator'),
]
