NB_SOUNDS = 30

O = 1
UW = 2
OW = 3
A = 4
AY = 5
AW = 6
I = 7
EY = 8
U = 9
E = 10
IW = 11
M = 12
B = 13
P = 14
L = 15
N = 16
D = 17
T = 18
G = 19
K = 20
R = 21
Z = 22
V = 23
F = 24
S = 25
C = 26
J = 27
H = 28
RW = 29
HW = 30
FIN = 31

to_sound = {
	'o':O,
	'uw':UW,
	'ow':OW,
	'a':A,
	'ay':AY,
	'aw':AW,
	'i':I,
	'ey':EY,
	'u':U,
	'e':E,
	'iw':IW,
	'm':M,
	'b':B,
	'p':P,
	'l':L,
	'n':N,
	'd':D,
	't':T,
	'g':G,
	'k':K,
	'r':R,
	'z':Z,
	'v':V,
	'f':F,
	's':S,
	'c':C,
	'j':J,
	'h':H,
	'rw':RW,
	'hw':HW,
}

to_letters = {}

for key in to_sound:
	to_letters[to_sound[key]]=key

def cast_to_sounds(word):
	k = 0
	sounds = []

	while k < len(word):
		key = word[k]

		if (k+1 < len(word)) and (word[k+1] in ['y','w']):
			key = key + word[k+1]
			k = k + 1

		if key in to_sound:
			sounds.append(to_sound[key])

		k = k + 1

	sounds.append(FIN)
	return sounds

def to_alphabetic(sounds):
	word = ""

	for sound in sounds:
		word = word + to_letters[sound]

	return word
