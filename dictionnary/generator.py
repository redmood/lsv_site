# -*- coding: utf-8 -*-

import numpy as np
from numpy.random import choice, seed
import time
seed(int(round(time.time())))
import codecs
from dictionnary import converter as cv
from dictionnary.matrix import M_SIZE

def generate_words(max_mots,word_size,p,in_database_words):
    """
        Randomly generates a list of words, based on the words
        contained in the database.

        :max_mots:type int: number of words to generate
        :word_size:type int: size of the words to generate (in number of sounds)
        :p:type numpy matrix 3x3: the probability matrix, to build words that
        sounds like the language of the given words (here it is morwuwhw words)
        :in_database_words:type list: list of the morwuwhw words in the
        database.
    """
    words = []
    for TGT in range(word_size,word_size+1):

    #K = 100
    #for TGT in range(4,11):
        nb_mots = 0
        while nb_mots<max_mots:
            i=0
            j=0
            res = []
            while not j == cv.FIN:
                #print("sum prob({},{}) : {}".format(i,j,p[i,j,:]))
                k=choice(range(M_SIZE),1,p=p[i,j,:])[0]
                res.append(k)
                i=j
                j=k
            if len(res) == 1+TGT:
                sounds=res[:-1]
                word=cv.to_alphabetic(sounds)
                if word in in_database_words:
                    word=word+"*"
                nb_mots += 1
                words.append(word)
    return words
