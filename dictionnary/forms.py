from django import forms
from django.core.validators import RegexValidator

class SearchWordForm(forms.Form):
    FRANCAIS = 'FR'
    MORWUWHW = 'MH'
    LANGUAGES = (
        (FRANCAIS, 'Français'),
        (MORWUWHW, 'Morwuwhw'),
    )

    alphanumeric = RegexValidator(
        regex=r'^[a-zàáâãäåòóôõöèéêëçìíîïùúûüÿ]*$',
        message='Votre mot ne doit contenir que des lettres minuscules'
    )

    word = forms.CharField(max_length=50,validators=[alphanumeric])
    language = forms.ChoiceField(choices=LANGUAGES)

class GeneratorForm(forms.Form):
    max_words = forms.IntegerField(min_value=1,max_value=100)
    size_words = forms.IntegerField(min_value=1,max_value=40)
