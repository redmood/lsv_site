from django.contrib import admin
from django.db import models
from django.forms import Textarea

from dictionnary.models import Word


@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols': 80})},
    }

    ordering = ('french',)

    list_display = [
        'word_link',
        'french',
        'morwuwhw',
        'precisions',
    ]

    fields = ['french', 'morwuwhw', 'precisions', 'entry_date']

    readonly_fields = ['word_link', 'entry_date']

    search_fields = ['french', 'morwuwhw']

    list_editable = ['french', 'morwuwhw', 'precisions']

    def word_link(self, instance):
        return f'mot {instance.id}'
