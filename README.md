#Personal website

Domain: liberte-securite-vieprivee.fr

Contains the following applications:

##Home

Contains the homepage of the website, plus the about page, where I briefly introduce myself, plus mandatory legal mentions.

##Curriculum

My Curriculum vitae in HTML version.

##Portfolio

A blog-like gallery of some personal programming projects.

##Personal Dictionnary project

A dictionnary web application to handle a vocabulary list. It is a french-morwuwhw dictionnary, morwuwhw being a conlang of mine. The dictionnary also have a generator functionnality, used to randomly generate new words. The generator is based on a "machine à mots" python code, from the youtuber of the "Sciences Étonnantes" channel. Il uses the words in the dictionnary, to generate words that looks like the words already existing in the language.

Licence : Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0).

##Characters

Meant to build a blog-like gallery list of fictive characters.

##Calculator

An API endpoint, built with Django rest framework.
