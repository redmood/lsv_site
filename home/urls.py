from django.urls import path, include
from home import views

app_name = 'home'

urlpatterns = [
    path('', views.home_page, name='home'),
    path('about/', views.about_view, name='about'),
    path('mentions-legales/', views.mentions_legales, name='mentions_legales'),
]
