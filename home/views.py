from django.shortcuts import render

def home_page(request):
    context = dict()
    return render(request, 'home/home_page.html', context)

def about_view(request):
    return render(request, 'home/about.html')

def mentions_legales(request):
    return render(request, 'home/mentions_legales.html')
